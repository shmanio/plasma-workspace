# translation of plasma_applet_battery.po to Galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# mvillarino <mvillarino@users.sourceforge.net>, 2007, 2008.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2009, 2011.
# Marce Villarino <mvillarino@kde-espana.es>, 2011, 2012, 2013, 2014.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2013, 2015, 2016, 2017.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019, 2020, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-11 02:55+0000\n"
"PO-Revision-Date: 2023-03-26 12:48+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: package/contents/ui/BatteryItem.qml:106
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:167
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"A capacidade da batería baixou ao %1% e debería substituírse. Contacte co "
"fabricante."

#: package/contents/ui/BatteryItem.qml:186
#, kde-format
msgid "Time To Full:"
msgstr "Tempo ata cargarse:"

#: package/contents/ui/BatteryItem.qml:187
#, kde-format
msgid "Remaining Time:"
msgstr "Tempo restante:"

#: package/contents/ui/BatteryItem.qml:192
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr "Estimando…"

#: package/contents/ui/BatteryItem.qml:204
#, kde-format
msgid "Battery Health:"
msgstr "Capacidade da batería:"

#: package/contents/ui/BatteryItem.qml:210
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:224
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "A batería está configurada para cargar ata aproximadamente o %1%."

#: package/contents/ui/BrightnessItem.qml:64
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/CompactRepresentation.qml:103
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:23 package/contents/ui/logic.js:29
#: package/contents/ui/main.qml:147
#, kde-format
msgid "Fully Charged"
msgstr "Carga completa"

#: package/contents/ui/logic.js:28
#, kde-format
msgid "Discharging"
msgstr "Descargándose"

#: package/contents/ui/logic.js:30
#, kde-format
msgid "Charging"
msgstr "Cargándose"

#: package/contents/ui/logic.js:32
#, kde-format
msgid "Not Charging"
msgstr "Sen cargar"

#: package/contents/ui/logic.js:35
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Non presente"

#: package/contents/ui/main.qml:120 package/contents/ui/main.qml:351
#, kde-format
msgid "Battery and Brightness"
msgstr "Batería e brillo"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "Brightness"
msgstr "Brillo"

#: package/contents/ui/main.qml:122
#, kde-format
msgid "Battery"
msgstr "Batería"

#: package/contents/ui/main.qml:122
#, kde-format
msgid "Power Management"
msgstr "Xestión da enerxía"

#: package/contents/ui/main.qml:154
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Batería ao %1%, sen cargar"

#: package/contents/ui/main.qml:156
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Batería ao %1%, enchufada pero descargándose"

#: package/contents/ui/main.qml:158
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Batería ao %1%, cargando"

#: package/contents/ui/main.qml:161
#, kde-format
msgid "Battery at %1%"
msgstr "Batería ao %1%"

#: package/contents/ui/main.qml:169
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "A fonte de enerxía non é potente dabondo para cargar a batería"

#: package/contents/ui/main.qml:173
#, kde-format
msgid "No Batteries Available"
msgstr "Non hai baterías dispoñíbeis"

#: package/contents/ui/main.qml:179
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 ata a carga completa"

#: package/contents/ui/main.qml:181
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "%1 restantes"

#: package/contents/ui/main.qml:184
#, kde-format
msgid "Not charging"
msgstr "Sen cargar"

#: package/contents/ui/main.qml:188
#, kde-format
msgid "Automatic sleep and screen locking are disabled"
msgstr "A suspensión e o bloqueo de pantalla automáticos están desactivados"

#: package/contents/ui/main.qml:193
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "Unha aplicación solicitou activar o modo de rendemento"
msgstr[1] "%1 aplicacións solicitaron activar o modo de rendemento"

#: package/contents/ui/main.qml:197
#, kde-format
msgid "System is in Performance mode"
msgstr "O sistema está en modo de rendemento"

#: package/contents/ui/main.qml:201
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "Unha aplicación solicitou activar o modo de aforro de enerxía"
msgstr[1] "%1 aplicacións solicitaron activar o modo de aforro de enerxía"

#: package/contents/ui/main.qml:205
#, kde-format
msgid "System is in Power Save mode"
msgstr "O sistema está en modo de aforro de enerxía"

#: package/contents/ui/main.qml:210
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr "Desprace para axustar o brillo da pantalla"

#: package/contents/ui/main.qml:312
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "O trebello de batería activou a inhibición para todo o sistema"

#: package/contents/ui/main.qml:364
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Non se puido activar o modo de %1"

#: package/contents/ui/main.qml:376
#, kde-format
msgid "&Show Energy Information…"
msgstr "Mo&strar información da enerxía…"

#: package/contents/ui/main.qml:378
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr ""
"Mostrar a porcentaxe de batería sobre a icona cando a carga non estea "
"completa"

#: package/contents/ui/main.qml:384
#, kde-format
msgid "&Configure Energy Saving…"
msgstr "&Configurar o aforro enerxético…"

#: package/contents/ui/PopupDialog.qml:117
#, kde-format
msgid "Display Brightness"
msgstr "Brillo da pantalla"

#: package/contents/ui/PopupDialog.qml:146
#, kde-format
msgid "Keyboard Brightness"
msgstr "Brillo do teclado"

#: package/contents/ui/PowerManagementItem.qml:38
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "Bloquear manualmente a suspensión e o bloqueo de pantalla"

#: package/contents/ui/PowerManagementItem.qml:71
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"O portátil está configurado para non suspenderse ao pechar a pantalla sempre "
"que haxa un monitor externo conectado."

#: package/contents/ui/PowerManagementItem.qml:84
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] ""
"%1 aplicación está bloqueando actualmente a suspensión e o bloqueo de "
"pantalla:"
msgstr[1] ""
"%1 aplicacións están bloqueando actualmente a suspensión e o bloqueo de "
"pantalla:"

#: package/contents/ui/PowerManagementItem.qml:106
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr ""
"%1 está bloqueando actualmente a suspensión e o bloqueo de pantalla (%2)"

#: package/contents/ui/PowerManagementItem.qml:108
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr ""
"%1 está bloqueando actualmente a suspensión e o bloqueo de pantalla (motivo "
"descoñecido)"

#: package/contents/ui/PowerManagementItem.qml:110
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr ""
"Unha aplicación está bloqueando actualmente a suspensión e o bloqueo de "
"pantalla (%1)"

#: package/contents/ui/PowerManagementItem.qml:112
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr ""
"Unha aplicación está bloqueando actualmente a suspensión e o bloqueo de "
"pantalla (motivo descoñecido)"

#: package/contents/ui/PowerManagementItem.qml:116
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:118
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: motivo descoñecido"

#: package/contents/ui/PowerManagementItem.qml:120
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr "Aplicación descoñecida: %1"

#: package/contents/ui/PowerManagementItem.qml:122
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "Aplicación descoñecida: motivo descoñecido"

#: package/contents/ui/PowerProfileItem.qml:34
#, kde-format
msgid "Power Save"
msgstr "Aforro de enerxía"

#: package/contents/ui/PowerProfileItem.qml:38
#, kde-format
msgid "Balanced"
msgstr "Equilibrado"

#: package/contents/ui/PowerProfileItem.qml:42
#, kde-format
msgid "Performance"
msgstr "Rendemento"

#: package/contents/ui/PowerProfileItem.qml:59
#, kde-format
msgid "Power Profile"
msgstr "Perfil de enerxía"

#: package/contents/ui/PowerProfileItem.qml:191
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"O modo de rendemento desactivouse para reducir a xeración de calor porque o "
"computador detectou que podería telo no colo."

#: package/contents/ui/PowerProfileItem.qml:193
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr ""
"O modo de rendemento non está dispoñíbel porque o computador está demasiado "
"quente."

#: package/contents/ui/PowerProfileItem.qml:195
#, kde-format
msgid "Performance mode is unavailable."
msgstr "O modo de rendemento non está dispoñíbel."

#: package/contents/ui/PowerProfileItem.qml:208
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Pode que se reduza o rendemento para reducir a xeración de calor porque o "
"computador detectou que podería telo no colo."

#: package/contents/ui/PowerProfileItem.qml:210
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr ""
"Pode que se reduza o rendemento porque o computador está demasiado quente."

#: package/contents/ui/PowerProfileItem.qml:212
#, kde-format
msgid "Performance may be reduced."
msgstr "Pode que se reduza o rendemento."

#: package/contents/ui/PowerProfileItem.qml:223
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "Unha aplicación solicitou activar o %2:"
msgstr[1] "%1 aplicacións solicitaron activar o %2:"

#: package/contents/ui/PowerProfileItem.qml:241
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#, fuzzy
#~| msgid "Power management is disabled"
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "A xestión da enerxía está desactivada"

#~ msgid "General"
#~ msgstr "Xeral"

#~ msgctxt "short symbol to signal there is no battery currently available"
#~ msgid "-"
#~ msgstr "-"

#~ msgid "Configure Power Saving..."
#~ msgstr "Configurar o aforro enerxético…"

#~ msgid "Time To Empty:"
#~ msgstr "Tempo ata baleirarse:"

#~ msgid ""
#~ "Disabling power management will prevent your screen and computer from "
#~ "turning off automatically.\n"
#~ "\n"
#~ "Most applications will automatically suppress power management when they "
#~ "don't want to have you interrupted."
#~ msgstr ""
#~ "Desactivando a xestión de enerxía evitará que a pantalla e o computador "
#~ "se apaguen de maneira automática.\n"
#~ "\n"
#~ "A meirande parte das aplicacións desactivarán automaticamente a xestión "
#~ "de enerxía cando non queiran que as interrompan."

#~ msgctxt "Some Application and n others are currently suppressing PM"
#~ msgid ""
#~ "%2 and %1 other application are currently suppressing power management."
#~ msgid_plural ""
#~ "%2 and %1 other applications are currently suppressing power management."
#~ msgstr[0] "%2 e outra aplicación máis están impedindo a xestión de enerxía."
#~ msgstr[1] "%2 e outras %1 aplicacións están impedindo a xestión de enerxía."

#~ msgctxt "Some Application is suppressing PM"
#~ msgid "%1 is currently suppressing power management."
#~ msgstr "%1 está impedindo a xestión de enerxía."

#~ msgctxt "Some Application is suppressing PM: Reason provided by the app"
#~ msgid "%1 is currently suppressing power management: %2"
#~ msgstr "%1 está impedindo a xestión de enerxía: %2"

#~ msgctxt "Used for measurement"
#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "%1% Charging"
#~ msgstr "%1% Cargando"

#~ msgid "%1% Plugged in"
#~ msgstr "%1% Enchufado"

#~ msgid "%1% Battery Remaining"
#~ msgstr "%1% restante de batería"

#~ msgctxt "The degradation in the battery's energy capacity"
#~ msgid "Capacity degradation:"
#~ msgstr "Degradación da capacidade:"

#~ msgctxt "Placeholder is battery's capacity degradation"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgctxt "Placeholder is battery capacity"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Vendor:"
#~ msgstr "Fabricante:"

#~ msgid "Model:"
#~ msgstr "Modelo:"

#~ msgid "No screen or keyboard brightness controls available"
#~ msgstr "Non se dispón de ningún control da pantalla nin do teclado"

#~ msgctxt "Placeholder is battery name"
#~ msgid "%1:"
#~ msgstr "%1:"

#~ msgid "N/A"
#~ msgstr "N/D"

#~ msgid "1 hour "
#~ msgid_plural "%1 hours "
#~ msgstr[0] "1 hora "
#~ msgstr[1] "%1 horas "

#~ msgid "1 minute"
#~ msgid_plural "%1 minutes"
#~ msgstr[0] "1 minuto"
#~ msgstr[1] "%1 minutos"

#, fuzzy
#~| msgid "AC Adapter:"
#~ msgid "AC Adapter"
#~ msgstr "Transformador de corrente:"

#, fuzzy
#~| msgid "Plugged in"
#~ msgid "Plugged In"
#~ msgstr "Enchufado"

#, fuzzy
#~| msgid "Not plugged in"
#~ msgid "Not Plugged In"
#~ msgstr "Non enchufado"

#, fuzzy
#~| msgctxt "Label for remaining time"
#~| msgid "Time Remaining:"
#~ msgid "Time remaining until full: %1"
#~ msgstr "Tempo restante:"

#, fuzzy
#~| msgctxt "Label for remaining time"
#~| msgid "Time Remaining:"
#~ msgid "Time remaining until empty: %1"
#~ msgstr "Tempo restante:"

#~| msgid "<b>%1% (charged)</b>"
#~ msgid "%1% (charged)"
#~ msgstr "%1% (cargada)"

#~ msgctxt "tooltip"
#~ msgid "AC Adapter:"
#~ msgstr "Transformador de corrente:"

#~ msgctxt "tooltip"
#~ msgid "<b>Plugged in</b>"
#~ msgstr "<b>Enchufado</b>"

#~ msgctxt "tooltip"
#~ msgid "<b>Not plugged in</b>"
#~ msgstr "<b>Non enchufado</b>"

#~ msgctxt "overlay on the battery, needs to be really tiny"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Configure Battery Monitor"
#~ msgstr "Configurar o vixilante da batería"

#~ msgctxt "Suspend the computer to RAM; translation should be short"
#~ msgid "Sleep"
#~ msgstr "Durmir"

#~ msgctxt "Suspend the computer to disk; translation should be short"
#~ msgid "Hibernate"
#~ msgstr "Hibernar"

#~ msgctxt "Battery is not plugged in"
#~ msgid "<b>Not present</b>"
#~ msgstr "<b>Non presente</b>"

#~ msgid "Show the state for &each battery present"
#~ msgstr "Mostrar o &estado de cada batería presente"

#~ msgid "<b>Battery:</b>"
#~ msgstr "<b>Batería:</b>"

#~ msgctxt "tooltip"
#~ msgid "<b>AC Adapter:</b>"
#~ msgstr "<b>Transformador de corrente</b>"

#~ msgctxt "tooltip"
#~ msgid "Not plugged in"
#~ msgstr "Non enchufado"

#, fuzzy
#~| msgid "Show the state for &each battery present"
#~ msgid "Show remaining time for the battery"
#~ msgstr "Mostrar o &estado de cada batería presente"

#~ msgctxt "tooltip: placeholder is the battery ID"
#~ msgid "<b>Battery %1:</b>"
#~ msgstr "<b>Batería %1:</b>"
